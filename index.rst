.. |label| replace:: CleanUp Scripte
.. |snippet| replace:: FvCleanUp
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.10
.. |Version| replace:: 1.0.2
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Mit diesem Plugin werden Artikel und Kunden über Cronjobs aufgeräumt. Die Datensätze können entweder deaktiviert oder gelöscht werden. Dabei wird verglichen ob das fv_olis_update Attribute während des Import gesetzt wurde und der Datensatz in diesem Zeitraum upgedatet wurde. Alle vom Update nicht betroffenen Datensäzte werden aufgeräumt.


Backend
-------

.. image:: FvCleanUp1.png


:Clean Up Artikel: Sie können den Clean Up deaktivieren oder Artikel deaktivieren oder löschen die nicht upgedatet wurden.
:Clean Up Hauptartikel anhand Varianten: Wenn ein Grundartikel keine Varianten besitzt oder alle deaktiviert sind wird der Grundartikel auch gelöscht oder deaktiviert, je nach Einstellung bei Clean Up Artikel.
:Clean Up Kunden: Sie können den Clean Up deaktivieren oder Kunden deaktivieren oder löschen die nicht upgedatet wurden. 


Cronjobs

.. image:: FvCleanUp2.png



technische Beschreibung
------------------------
Je nach Einstellung der Cronjobs wird dies in dem hinterlegten Intervall durchgeführt.





